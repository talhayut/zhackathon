// Install the ServiceWorker
self.addEventListener("install", function(event) {
  event.waitUntil(
    // Open a cache
    caches.open("v1").then(function(cache) {
      // Define what we want to cache
      return cache.addAll([
        "index.html",
        "zhack.ico",
        "manifest.json",
        "img/zhack-512.png",
        "img/zhack-192.png"
      ]);
    })
  );
});

// Use ServiceWorker (or not) to fetch data
self.addEventListener("fetch", event => {
  event.respondWith(
    // Look for something in the cache that matches the request
    caches.match(event.request).then(response => {
      // If we find something, return it
      // Otherwise, use the network instead
      return response || fetch(event.request);
    })
  );
});

let deferredPrompt;

self.addEventListener("beforeinstallprompt", event => {
  event.preventDefault();
  deferredPrompt = event;
  btnAdd.style.display = "block";

  btnAdd.addEventListener("click", e => {
    // hide our user interface that shows our A2HS button
    btnAdd.style.display = "none";
    // Show the prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then(choiceResult => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the A2HS prompt");
      } else {
        console.log("User dismissed the A2HS prompt");
      }
      deferredPrompt = null;
    });
  });
});


self.addEventListener('push', function(event) {
  const body = event.data.text();
  const title = 'ZERTO Hackathon';
  const options = {
    body,
    icon: 'images/icon.png',
    badge: 'images/badge.png'
  };

  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${body}"`);

  const notificationPromise = self.registration.showNotification(title, options);
  event.waitUntil(notificationPromise);
});

self.addEventListener('notificationclick', function(event) {
  console.log('[Service Worker] Notification click Received.');
  event.notification.close();
});
