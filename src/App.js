import React, { Component } from "react";
import NavBar from "./components/NavBar/NavBar";
import { Intro } from "./components/Slabs/Intro";
import { ProjectsContainer } from "./components/Slabs/ProjectsContainer";
import { LocationContainer } from "./components/Slabs/LocationContainer";
import { ScheduleCotainer } from "./components/Slabs/ScheduleContainer";
class App extends Component {
  render() {
    return (
      <div className="main">
        <NavBar />
        <Intro />
        <ProjectsContainer db={this.props.db} />
        <ScheduleCotainer />
        <LocationContainer />
      </div>
    );
  }
}

export default App;
