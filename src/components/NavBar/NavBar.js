import React from "react";
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from "reactstrap";
import "./NavBar.css";
import { isMobile } from "react-device-detect";

export default class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleMobile = this.toggleMobile.bind(this);

    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleMobile() {
    return isMobile && this.toggle();
  }

  render() {
    return (
      <Navbar dark expand="md">
        <NavbarBrand id="brand" href="#home">
          <img className="brand_img" src={"https://gitlab.com/uploads/-/system/project/avatar/10086687/zhack-512.png?width=160"} alt="zhack-brand" />
          Hackathon
        </NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar onClick={this.toggleMobile}>
            <NavItem>
              <NavLink href="#home">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#projects">Projects</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#schedule">Schedule</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#location">Where is it</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}
