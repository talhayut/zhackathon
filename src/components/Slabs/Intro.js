import React from "react";
import { Jumbotron } from "reactstrap";
import { Particle} from '../Particle/Particle';

export const Intro = () => {
  return (
    <div className="section section_first" id="home">
      <Jumbotron className="jumbo">
        <Particle />
        <h1 className="display-3">
          Welcome to <span>ZHackathon</span>
        </h1>
        <p className="lead">
          This is where we track projects, collaborate, and find team mates.
        </p>
        <hr className="my-2" />
        <h6>make sure to go in from you mobile and approve the prompt to make this an app on your mobile!</h6>
      </Jumbotron>
    </div>
  );
};
