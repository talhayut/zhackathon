import React from "react";
import { Jumbotron } from "reactstrap";
import Projects from "../Projects/Projects";

export class ProjectsContainer extends React.Component {
  render() {
    return (
      <div className="section section_second" id="projects">
        <Jumbotron className="jumbo">
          <h1 className="progress_header">
            Projects
            <span role="img" aria-label="hammer_emoji">
              <img alt="projects" className="header_img" src={"https://www.svgrepo.com/show/299320/hammer-wrench.svg"} />
            </span>
          </h1>
          <Projects db={this.props.db} />
        </Jumbotron>
      </div>
    );
  }
}
