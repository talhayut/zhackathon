import React from "react";
import  WhereIsIt from '../Location/WhereIsIt';

export const LocationContainer = () => {
  return (
    <div className="section location" id="location">
        <WhereIsIt />
    </div>
  );
};
