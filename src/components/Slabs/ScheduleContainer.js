import React from "react";
import Schedule from "../Schedule/Schedule";

export const ScheduleCotainer = () => {
  return (
    <div className="section section_schedule schedule" id="schedule">
        <Schedule />
    </div>
  );
};
