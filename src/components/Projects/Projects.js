import React from "react";
import SwipeableViews from "react-swipeable-views";
import uniqueId from "uniqid";

export default class Projects extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      projects: []
    };

    this.root = this.props.db.ref("/");
    this.uniq = uniqueId();

    this.styles = {
      slide: {
        padding: 0,
        minHeight: 100,
        color: "#fff"
      },
      slide1: {
        backgroundColor: "#6e6e6e",
        height: "500px",
        margin: "10px",
        borderRadius: "20px",
        border: "1px solid white"
      }
    };
  }

  generateParticipant = ({ participants }) => {
    return (
      <SwipeableViews enableMouseEvents>
        {participants.map((participant, key) => (
          <div className="participant" key={key}>
            <i className="arrow left" />
            <label>{participant.name}</label>
            {participant.img && (
              <img alt="participant_img" className="participant_img" src={participant.img} />
            )}
            <i className="arrow right" />
          </div>
        ))}
      </SwipeableViews>
    );
  };

  generateProjectCard = () => {
    if (this.state.projects) {
      return this.state.projects.map((project, key) => {
        return (
          <div
            key={key}
            style={Object.assign({}, this.styles.slide, this.styles.slide1)}
            className="zproject">
            <label className="project_header">{project.name}</label>
            
            <div className="project_description">{project.description}</div>
            
            <div className="project_participants">
            <label className="project_participants__label">Participants</label>
            {this.generateParticipant(project)}</div>
          </div>
        );
      });
    }
  };

  componentWillMount() {
    this.root.on("value", data => {
      const projects = Object.values(data.val());

      const previousProjects = projects[0].projects.map(project => {
        return project;
      });

      this.setState({
        projects: [...previousProjects]
      });
    });
  }

  render() {
    return (
      this.state.projects && (
        <div className="projects" id="projects">
          <SwipeableViews enableMouseEvents>{this.generateProjectCard()}</SwipeableViews>
        </div>
      )
    );
  }
}
