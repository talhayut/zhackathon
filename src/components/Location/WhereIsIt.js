import React from "react";
import { Jumbotron } from "reactstrap";

export default class WhereIsIt extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <Jumbotron className="jumbo">
          <h1 className="progress_header">
            Where Is It{" "}
            <span role="img" aria-label="world_emoji">
              <img
                alt="location"
                className="location_img"
                src={"https://image.flaticon.com/icons/svg/282/282114.svg"}
              />
            </span>
          </h1>
        </Jumbotron>
        <iframe
          className="location_map"
          title="whereisit"
          frameBorder="0"
          style={{ height: "70vh", width: "100%", paddingBottom: "50px", textAlign: "center" }}
          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAbMVzt-3babkdqa37QE7n2Aq-wat8P_5Y&q=nyx+herzliya"
          allowFullScreen
        />
      </div>
    );
  }
}
