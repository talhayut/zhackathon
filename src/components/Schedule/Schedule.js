import React from "react";
import { Jumbotron } from "reactstrap";
import Timeline from "react-time-line";

export default class Schedule extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const events = [
      { ts: "2018-12-26T07:00:00.007Z", text: "ZHackathon Kickoff!" },
      { ts: "2018-12-26T09:00:00.000Z", text: "Lunch" },
      { ts: "2018-12-25T22:30:43.654Z", text: "Sweets and beers" },
      { ts: "2018-12-26T13:30:00.000Z", text: "Massage" },
      { ts: "2018-12-27T10:00:46.587Z", text: "Semi Finals!" },
      { ts: "2018-12-27T16:20:46.587Z", text: "FINALS!!!" },
      { ts: "2018-12-28T23:00:00.000Z", text: "Code" },
      { ts: "2018-12-28T10:00:46.587Z", text: "Free Time!" },
      { ts: "2018-12-29T16:20:46.587Z", text: "Woohoo!!!" }
    ];

    return (
      <div id="schedule">
        <Jumbotron className="jumbo">
          <h1 className="schedule_header">
            Schedule{" "}
            <span role="img" aria-label="schedule_emoji">
            <span role="img" aria-label="hammer_emoji">
              <img alt="schedule" className="schedule_img" src={"https://visualpharm.com/assets/905/Calendar%2027-595b40b65ba036ed117d29f0.svg"} />
            </span>
            </span>
          </h1>
        </Jumbotron>
        <Timeline items={events} style={{ height: "auto" }} />
      </div>
    );
  }
}
