import React from "react";
import Particles from "react-particles-js";
import * as particleConfig from "../../assets/particlesjs-config.json";
import * as particlePolyConfig from "../../assets/particlesjs-config-poly-test.json";
import { isMobile } from "react-device-detect";

export const Particle = () => {
  const particle = config => {
    return <Particles canvasClassName="canvas" params={config} />;
  };

  const renderParticles = () => {
    return isMobile ? particle(particlePolyConfig) : particle(particleConfig);
  };

  return renderParticles();
};
