import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import firebase from "firebase";
import { BrowserRouter } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import registerServiceWorker from "./registerServiceWorker";
import "./App.css";

const config = {
  apiKey: "AIzaSyBl3OGVhhc6XvcYmnAJQY-AsNJujta1StY",
  authDomain: "zhackathon-9d636.firebaseapp.com",
  databaseURL: "https://zhackathon-9d636.firebaseio.com",
  projectId: "zhackathon-9d636",
  storageBucket: "zhackathon-9d636.appspot.com",
  messagingSenderId: "472515746286"
};

firebase.initializeApp(config);

const db = firebase.database();

registerServiceWorker();

ReactDOM.render(
  <BrowserRouter>
    <App db={db} />
  </BrowserRouter>,
  document.getElementById("root")
);
